# AwesomeWM

After 3 years of [xmonading](https://xmonad.org/), i wanted to give
[AwesomeWM](https://awesomewm.org/) a fresh try so here we go

## Overview

![scrot](scrot/current.png)

## Some notes

- This configuration use nerd patched font to get all the icons
- Main theme is Gruvbox (see `themes` folder)
- Wallpaper credits goes to [McSinyx](https://github.com/McSinyx/dotfiles)
- Use of YAWL (yet another widget library, included in this configuration see `yawl` folder)

## License

See `LICENSE` file
