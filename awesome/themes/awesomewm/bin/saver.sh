#!/usr/bin/env zsh

revert() {
  rm /tmp/*screen.png
  xset dpms 0 0 0
}
trap revert HUP INT TERM
xset +dpms dpms 0 0 5
scrot /tmp/screen.png
convert -blur 0x8 /tmp/screen.png /tmp/lscreen.png
[[ !(-n "pidof i3lock") ]] && i3lock -u -i /tmp/lscreen.png
revert
