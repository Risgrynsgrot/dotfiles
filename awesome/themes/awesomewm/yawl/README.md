# YAWL (yet another widget library)

This is my shot to create an Awesome Widget Library. Since I dislike all the
other one I found, I created mine.

Consider all this as **experimental**.

## Overview

All the things are wrapped into module.

Each module in the widget folder provides it's own widget.

Each widget is made of two component, an icon and some text.

## Usage

Let's consider the battery widget

To use it, ensure `yawl` is in the awesome config directory, then add

```lua
local battery = require("yawl.widgets.battery")

[..]

s.mywibox:setup {
    { -- Right widgets
        battery.widget(),
    },
}
```

### Configuration

Widgets colors can be configured using the following variables in your `theme.lua`

```lua
-- YAWL required theme settings
theme.yawl_font           = theme.font
theme.yawl_bg             = "#458588" -- default bg
theme.yawl_bg_ok          = "#98971a" -- no email
theme.yawl_bg_nok         = "#fb4934" -- email in mailbox
theme.yawl_fg             = "#FFFFFF" -- default fg
theme.yawl_spotify_absent = theme.bg_normal -- bg when spotify not running
theme.yawl_spotify_pause  = "#d79921" -- bg when spotify running (paused)
theme.yawl_spotify_play   = "#d3869b" -- bg when spotify running (playing)
theme.yawl_battery_full   = "#b8bb26" -- battery full + 70%
theme.yawl_battery_mid    = "#d79921" -- battery medium 30% - 70%
theme.yawl_battery_low    = "#fb4934" -- battery low - 30%
```

Mail widget take path to watched imap folder as an argument

```lua
mails.widget("/home/papey/mails/bt/Inbox/new"),
```

## Dependencies

Here is a list of some dependencies required by some modules

- spotify : requires [playerctl](https://github.com/altdesktop/playerctl) to get current playing status
- pomodoro : requires [calabash](https://github.com/papey/calabash) to track pomodoro sessions and `dkjson` to parse calabash client output
