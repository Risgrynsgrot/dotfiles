-- YAWL, Base widget module
local base = {}

-- Requires
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Draw a simple icon from a font (nerd font ftw)
function base.icon(char)
    return wibox.widget {
        resize = true,
        widget = wibox.widget.textbox,
        font = beautiful.icon_font,
        text = " " .. char .. " "
    }
end

-- Draw a simple separator
function base.separator()
    return wibox.widget {
        widget = wibox.widget.separator,
        orientation = "vertical",
        forced_width = 10,
        color = beautiful.fg_normal,
        visible = true
    }
end

-- Get a fresh text wibox
function base.txt()
    return wibox.widget{
        font = beautiful.yawl_font,
        widget = wibox.widget.textbox,
    }
end

-- Get a fresh background wibox
function base.bg()
    return wibox.widget{
        widget = wibox.container.background,
        bg = beautiful.yawl_bg,
        fg = beautiful.yawl_fg,
    }
end

-- Return the module
return base
