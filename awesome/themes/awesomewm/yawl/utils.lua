-- YAWL, utils module
local utils = {}

-- strim, for simple trim
function utils.strim(char)
    return char:gsub("\n", "")
end

-- count line number
function utils.lc(str)
    return select(2, str:gsub("\n", "\n"))
end

-- cut line
function utils.cut(str)
    return str:sub(1, 130)
end

-- sanitize line with trim and cut
function utils.sanitize(str)
    local temp = utils.strim(str)
    local temp = utils.cut(temp)
    return temp
end

-- split is used to split a line using a regex
function utils.split(str, regex)
    local content = {}
    for s in str:gmatch(regex) do
      table.insert(content, s)
    end
    return content
end

-- run command and get output
function utils.run(cmd)
    local reader = io.popen(cmd, "r")
    local content = reader:read('*a')
    reader:close()
    return content
end

-- Return module
return utils
