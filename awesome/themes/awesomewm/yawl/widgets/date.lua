-- YAWL, Date widget module
local date = {}
local mt = {}

-- Requires
local wibox = require("wibox")
local base = require("yawl.base")
local utils = require("yawl.utils")
local watch = require("awful.widget.watch")

-- Entrypoint
function mt.__call()

  -- base
  local t = base.txt()
  local w = base.bg()
  w:set_widget(t)

  -- icon widget
  local i = base.icon("")

  -- merge of the two
  local widget = wibox.widget {
    i,
    w,
    layout = wibox.layout.fixed.horizontal,
  }

  -- watch func
  watch(
    'date "+%H:%M - %d/%m/%y"', 10,
    function(_, stdout, stderr, exitreason, exitcode)
      t:set_text(" " .. utils.strim(stdout) .. " ")
    end,
    w
  )

  return widget

end

-- Return widget
return setmetatable(date, mt)
